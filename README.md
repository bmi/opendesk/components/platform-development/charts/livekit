<!--
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-License-Identifier: Apache-2.0
-->
# LiveKit Helm Charts

This repository contains Helm chart for deploying LiveKit related services to Kubernetes.


## Prerequisites

Before you begin, ensure you have met the following requirements:

- Kubernetes 1.21+
- Helm 3.0.0+
- Object Storage


## Documentation

The documentation is placed in the README of each helm chart:

- [livekit-meet](charts/livekit-meet)

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright © 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
