## [1.0.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/livekit/compare/v1.0.1...v1.0.2) (2024-06-24)


### Bug Fixes

* **livekit-meet:** Add additionalLabels to deployment ([8cdab3b](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/livekit/commit/8cdab3bef2680acdfd795348c3cc66872eaade15))

## [1.0.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/livekit/compare/v1.0.0...v1.0.1) (2024-06-24)


### Bug Fixes

* **livekit-meet:** Add url for livekit backend ([9ae772f](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/livekit/commit/9ae772fcf9703ef8feb4560de618bc730b4c3424))

# 1.0.0 (2024-06-24)


### Features

* **livekit-meet:** Add livekit-meet chart ([7a309c8](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/livekit/commit/7a309c884b4af8091eca8349b9c9268e6a32fa44))
